﻿using System;
using System.Threading;
namespace cSharpEvents
{
    public class VideoEventArgs : EventArgs
    {
        public Video Video { get; set; }
    }

    public class VideoEncoder
    {
        //Define a delegate
        public delegate void VideoEncodedEventHandler(object source, VideoEventArgs args);
        //Define an event based on that delegate
        public event VideoEncodedEventHandler VideoEncoded;

        /********** The delagate and event base can be replaced by the NEW:
         public event EventHandler<VideoEventArgs> VideoEncoded;
         ***********/

        public void Encode(Video video)
        {
            Console.WriteLine("Encoding Video... ");
            Thread.Sleep(3000);
            //We call the method for the Event. 
            OnVideoEncoded(video);
        }

        //Raise the event.
        protected virtual void OnVideoEncoded(Video video)
        {
            //If 'VideoEncoded' is not empty, means that someone has subscribed to that event.
            //which means that we have a pointer to an event handler method.
            if (VideoEncoded != null)
                //We call the event handler.
                VideoEncoded(this, new VideoEventArgs(){ Video = video});
        }
    }
}
