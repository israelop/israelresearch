﻿using System;
namespace cSharpEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            var video = new Video() { Title = "Video 1" };
            var videoEncoder = new VideoEncoder(); //publisher
            var mailService = new MailService(); // subscriber
            var messageService = new MessageService(); //subscriber

            //We use the '+=' operator to register the handler for the evnet.
            //The handler is this method and the MailService.
            //We don't use '()' after 'OnVideoEncoded' because it is a reference or a pointer to method.
            videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded += messageService.OnVideoEncoded;

            videoEncoder.Encode(video);
        }
    }
}