# Radar Chart (Spider Web Graph)

## Abstract

A user visual interface to allow the user set path planning intent paramenters and then show the realistic parameters determined by Meher's path planning algorithm.

- RadarChart
![image](https://drive.google.com/uc?export=view&id=1W62BPBevnXOy3ArTMEYQjOiZc74xADY7)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Download and Install
```
- Unity 2017.4.0f1 (64-bit) or a newer version.
- Visual Studio 2017
```

## Deployment
```
- Open Unity
- Open Project: RadarChart
- Wait until the project finish loading in Unity

In Unity:
- Press the play button to see the simulatio in the Game window.

Crete a Stand Alone Executable
- Open File > Build Settings window.
- Click Add Open Scenes to add the scene (Look for "Main").
- Press "Build" Button.
- Create a new folder somewhere else than our current project folder.
```

## Running the Stand Alone Application
```
- Inside the new folder you'll find three things. A "<StandAloneFolderName>_Data" folder, 
"<StandAloneFolderName>.exe" file, and "UnityPlayer.dll" file.

- Execute the "<StandAloneFolderName>.exe" file, and check the "Windowed" box

- After setting the parameters and press the "SET INTENT" button, two files will be created.

- Inside "<StandAloneFolderName>_Data" folder > "StreamingAssets" you'll find "data.txt" 
and "intentData.txt" files.

- "data.txt" is the INPUT FILE to receive the realistic parameters from the path planning algorithm. 
The application reads this file in each frame (loop). If you change the value of any a parameter 
in this file, the graph will change dinamically (Only numbers from 0 - 10 are supported, if you 
enter other characters the program will crash).

- "intentData.txt" file stays the same. Its purpose is to show the users original intent. 
```
## Author

* **Israel Lopez**
