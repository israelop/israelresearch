﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using System;
using System.Text;

public class TestMesh : MonoBehaviour {

    public Material material;
    public Material clickMaterial;

    private RadioChartParams radioParams;
    private Vector2 mousePosition;

    private GameObject efficiencyPin;
    private GameObject stealthlyPin;
    private GameObject quicklyPin;
    private GameObject unknownPin;

   // private Boolean isLocked = false;
    public Texture btnTexture;

    void Start ()
    {
        startGame();
        drawClickableObjects();
        drawRadioChart();
        drawAxis();
        //LoadData();
    }

    void Update ()
    {
        updateRadioChart();
        if (radioParams.IsLocked)
        {
            LoadData();
            updateClickableObjects();
        }
    }

    void startGame()
    {
        radioParams = RadioChartParams.GetInstance;
    }

    public void drawRadioChart()
    {
        Mesh tMesh = new Mesh();
        GetComponent<MeshFilter>().mesh = tMesh;
        Vector3[] vertices = new Vector3[5];

        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, 1 * radioParams.Efficiency, 0);
        vertices[2] = new Vector3(1 * radioParams.Stealthly, 0, 0);
        vertices[3] = new Vector3(0, -1 * radioParams.Quickly, 0);
        vertices[4] = new Vector3(-1 * radioParams.Unknown, 0, 0);

        int[] triangles = new int[]
        {
            0, 1, 2,
            0, 2, 3,
            0, 3, 4,
            0, 4, 1
        };

        tMesh.Clear();
        tMesh.vertices = vertices;
        tMesh.triangles = triangles;
        GetComponent<MeshRenderer>().material = material;
    }

    public void updateRadioChart()
    {
        drawRadioChart();
    }

    public void drawAxis()
    {
        GameObject xAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);
        xAxis.name = "xAxis";
        xAxis.transform.position = new Vector3(0, 0, 0);
        xAxis.transform.localScale = new Vector3(20, 0.05f, 0.2f);

        GameObject yAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);
        yAxis.name = "yAxis";
        yAxis.transform.position = new Vector3(0, 0, 0);
        yAxis.transform.localScale = new Vector3(0.05f, 20, 0.2f);

        for (int i = -10; i <= 10; i++)
        {
            GameObject xMarker = GameObject.CreatePrimitive(PrimitiveType.Cube);
            string markerXName = string.Format("Marker X: " + i);
            xMarker.name = markerXName;
            xMarker.transform.position = new Vector3(i, 0, 0);
            xMarker.transform.localScale = new Vector3(0.05f, 1, 0.2f);

            GameObject yMarker = GameObject.CreatePrimitive(PrimitiveType.Cube);
            string markerYName = string.Format("Marker Y: " + i);
            yMarker.name = markerYName;
            yMarker.transform.position = new Vector3(0, i, 0);
            yMarker.transform.localScale = new Vector3(1, 0.05f, 0.2f);
        }
    }

    public void drawClickableObjects()
    {
        efficiencyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        efficiencyPin.name = "efficiencyPin";
        efficiencyPin.AddComponent(Type.GetType("DragObject"));
        efficiencyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        efficiencyPin.transform.position = new Vector3(0, radioParams.Efficiency, 0);
        efficiencyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        stealthlyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        stealthlyPin.name = "stealthlyPin";
        stealthlyPin.AddComponent(Type.GetType("DragObject"));
        stealthlyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        stealthlyPin.transform.position = new Vector3(radioParams.Stealthly, 0, 0);
        stealthlyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        quicklyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        quicklyPin.name = "quicklyPin";
        quicklyPin.AddComponent(Type.GetType("DragObject"));
        quicklyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        quicklyPin.transform.position = new Vector3(0, -1 * radioParams.Quickly, 0);
        quicklyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        unknownPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        unknownPin.name = "safetyPin";
        unknownPin.AddComponent(Type.GetType("DragObject"));
        unknownPin.GetComponent<MeshRenderer>().material = clickMaterial;
        unknownPin.transform.position = new Vector3(-1 * radioParams.Unknown, 0, 0);
        unknownPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }

    public void updateClickableObjects()
    {
        efficiencyPin.transform.position = new Vector3(0, radioParams.Efficiency, 0);
        stealthlyPin.transform.position = new Vector3(radioParams.Stealthly, 0, 0);
        quicklyPin.transform.position = new Vector3(0, -1 * radioParams.Quickly, 0);
        unknownPin.transform.position = new Vector3(-1 * radioParams.Unknown, 0, 0);
    }

    public void LoadData()
    {
        string dataFileName = "data.txt";
        string filePath = Path.Combine(Application.streamingAssetsPath, dataFileName);
        int[] parameters = new int[4];
        string line;
        int counter = 0;

        StreamReader file = new StreamReader(filePath); 

        if (File.Exists(filePath))
        {
            while ((line = file.ReadLine()) != null)
            {
                parameters[counter] = int.Parse(line);
                Debug.Log(parameters[counter]);
                counter++;
            }

            radioParams.Efficiency = parameters[0];
            radioParams.Stealthly = parameters[1];
            radioParams.Quickly = parameters[2];
            radioParams.Unknown = parameters[3];

            file.Close();
            Debug.Log("There were " + counter + " lines.");
            Debug.Log("\n");
        }
    }

    public void CreateExternalFiles()
    {
        string dataFileName = "data.txt";
        string intentDataFileName = "intentData.txt";
        string dataFilePath = Path.Combine(Application.streamingAssetsPath, dataFileName);
        string intentFilePath = Path.Combine(Application.streamingAssetsPath, intentDataFileName);

        try
        {
            if (File.Exists(dataFilePath))
            {
                File.Delete(dataFilePath);
            }

            if (File.Exists(intentFilePath))
            {
                File.Delete(intentFilePath);
            }

            using (FileStream fs = File.Create(dataFilePath))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(radioParams.Efficiency 
                    + Environment.NewLine + radioParams.Stealthly 
                    + Environment.NewLine + radioParams.Quickly 
                    + Environment.NewLine + radioParams.Unknown);
                fs.Write(info, 0, info.Length);
            }

            using (FileStream fs = File.Create(intentFilePath))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(radioParams.Efficiency
                    + Environment.NewLine + radioParams.Stealthly
                    + Environment.NewLine + radioParams.Quickly
                    + Environment.NewLine + radioParams.Unknown);
                fs.Write(info, 0, info.Length);
            }
        }

        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }

    void OnGUI()
    {
        if (!btnTexture)
        {
            Debug.LogError("Please assign a texture on the inspector");
            return;
        }

        if (GUI.Button(new Rect(700, 50, 100, 40), "SET INTENT"))
        {
            CreateExternalFiles();
            radioParams.IsLocked = true;
            Debug.Log("Clicked the button with text");
        }
    }
}
