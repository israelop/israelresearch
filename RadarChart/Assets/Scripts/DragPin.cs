﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragPin : MonoBehaviour
{
    private Camera cam;
    private Vector3 point;
    private ChartParams chartParams = ChartParams.GetInstance;
    private RadarChart radarChart;


    void Start()
    {
        cam = Camera.main;
    }

    private void OnMouseDown()
    {
        Debug.Log("Clicking");
    }

    private void OnMouseDrag()
    {
        // point = mousePosition in the world
        if (!chartParams.IsLocked)
        {
            Debug.Log("Dragging");

            // if the the script is attached to the "efficientlyPin" GameObject
            if (name.Equals("efficientlyPin"))
            {
                if (point.y > 10)
                {
                    transform.position = new Vector3(transform.position.x, 10, transform.position.z);
                }
                else if (point.y < 0)
                {
                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                }
                else if (point.y >= 0 && point.y <= 10)
                {
                    transform.position = new Vector3(transform.position.x, (int)point.y, transform.position.z);
                }

                chartParams.Efficiently = (int)transform.position.y;
            }

            // if the the script is attached to the "stealthelyPin" GameObject
            else if (name.Equals("stealthelyPin"))
            {
                if (point.x > 10)
                {
                    transform.position = new Vector3(10, transform.position.y, transform.position.z);
                }
                else if (point.x < 0)
                {
                    transform.position = new Vector3(0, transform.position.y, transform.position.z);
                }
                else if (point.x >= 0 && point.x <= 10)
                {
                    transform.position = new Vector3((int)point.x, transform.position.y, transform.position.z);
                }

                chartParams.Stealthly = (int)transform.position.x;
            }

            // if the the script is attached to the "quicklyPin" GameObject
            else if (name.Equals("quicklyPin"))
            {
                if (point.y > 0)
                {
                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                }
                else if (point.y < -10)
                {
                    transform.position = new Vector3(transform.position.x, -10, transform.position.z);
                }
                else if (point.y >= -10 && point.y <= 0)
                {
                    transform.position = new Vector3(transform.position.x, (int)point.y, transform.position.z);
                }

                chartParams.Quickly = -(int)transform.position.y;
            }

            // if the the script is attached to the "safelyPin" GameObject
            else if (name.Equals("safelyPin"))
            {
                if (point.x > 0)
                {
                    transform.position = new Vector3(0, transform.position.y, transform.position.z);
                }
                else if (point.x < -10)
                {
                    transform.position = new Vector3(-10, transform.position.y, transform.position.z);
                }
                else if (point.x >= -10 && point.x <= 0)
                {
                    transform.position = new Vector3((int)point.x, transform.position.y, transform.position.z);
                }

                chartParams.Safely = -(int)transform.position.x;
            }
        }
    }

    void OnGUI()
    {
        point = new Vector3();
        Event currentEvent = Event.current;
        Vector2 mousePos = new Vector2();

        mousePos.x = currentEvent.mousePosition.x;
        mousePos.y = cam.pixelHeight - currentEvent.mousePosition.y;

        point = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 25));

        GUILayout.BeginArea(new Rect(20, 20, 300, 120));
        GUILayout.Label("Mouse position (x, y, z): " + point.ToString("F3"));
        GUILayout.EndArea();
    }
}
