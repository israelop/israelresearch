﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Example : MonoBehaviour {
    

    public Material material;
    public Transform axisBar;
    public int axisNum;

    private float angleNum;
    private Dictionary<string, float> data = new Dictionary<string, float>();
    private List<Vector3> points = new List<Vector3>();

    // Use this for initialization

    void Start () {

        float rotationAngle = 0;
        angleNum = 360 / axisNum;
        getData();
        //points.Add(transform.position);

        Vector3[] vertices = new Vector3[4];
        Vector2[] uv = new Vector2[4];
        int[] triangles = new int[6];

        //vertices[0] = new Vector3(0, 1);
        //vertices[1] = new Vector3(1, 1);
        //vertices[2] = new Vector3(0, 0);
        //vertices[3] = new Vector3(1, 0);

        for (int i = 0; i < axisNum; i++)
        {
            //Transform axis = Instantiate(axisBar, transform.position, Quaternion.identity)
            //    as Transform;
            Debug.Log("Assignment " + i + ": " + points[i]);
            vertices[i] = points[i];
        }

        uv[0] = new Vector2(0, 1);
        uv[1] = new Vector2(1, 1);
        uv[2] = new Vector2(0, 0);
        uv[3] = new Vector2(1, 0);

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 1;
        triangles[5] = 3;


        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);

        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = new Vector3(0, 0.5f, 0);

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = new Vector3(0, 1.5f, 0);

        GameObject capsule = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        capsule.transform.position = new Vector3(2, 1, 0);

        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cylinder.transform.position = new Vector3(-2, 1, 0);


        Mesh mesh = new Mesh();

        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;

        GameObject gameObject = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer));
        gameObject.transform.localScale = new Vector3(30, 30, 1);

        gameObject.GetComponent<MeshFilter>().mesh = mesh;
        gameObject.GetComponent<MeshRenderer>().material = material;


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void getData()
    {
        for(int i = 0; i < axisNum; i++)
        {
            data.Add(("Start " + (i + 1)), Random.Range(25, 93));
        }
        
        for(int i = 0; i < axisNum; i++)
        {
            points.Add(new Vector3(Random.Range(1, 5), 0, Random.Range(1, 5)));
            Debug.Log("Vertice " + i + ": " + points[i]);
        }
    }
}
