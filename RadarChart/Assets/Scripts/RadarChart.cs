﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using System;
using System.Text;

public class RadarChart : MonoBehaviour {

    public Material material;
    public Material clickMaterial;

    private ChartParams chartParams;
    private Vector2 mousePosition;

    private GameObject efficientlyPin;
    private GameObject stealthlyPin;
    private GameObject quicklyPin;
    private GameObject safelyPin;

    public Texture btnTexture;


    // Initialize values and gameObjects
    void Start ()
    {
        StartGame();
        DrawClickablePins();
        DrawRadarChart();
        DrawAxisBars();
    }

    void StartGame()
    {
        chartParams = ChartParams.GetInstance;
    }

    public void DrawRadarChart()
    {
        Mesh tMesh = new Mesh();
        GetComponent<MeshFilter>().mesh = tMesh;
        Vector3[] vertices = new Vector3[5];

        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, 1 * chartParams.Efficiently, 0);
        vertices[2] = new Vector3(1 * chartParams.Stealthly, 0, 0);
        vertices[3] = new Vector3(0, -1 * chartParams.Quickly, 0);
        vertices[4] = new Vector3(-1 * chartParams.Safely, 0, 0);

        int[] triangles = new int[]
        {
            0, 1, 2,
            0, 2, 3,
            0, 3, 4,
            0, 4, 1
        };

        tMesh.Clear();
        tMesh.vertices = vertices;
        tMesh.triangles = triangles;
        GetComponent<MeshRenderer>().material = material;
    }

    // Update chart values every frame
    void Update ()
    {
        UpdateRadioChart();
        if (chartParams.IsLocked)
        {
            LoadData();
            UpdateClickableObjects();
        }
    }

    public void UpdateRadioChart()
    {
        DrawRadarChart();
    }

    public void DrawAxisBars()
    {
        GameObject xAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);
        xAxis.name = "xAxis";
        xAxis.transform.position = new Vector3(0, 0, 0);
        xAxis.transform.localScale = new Vector3(20, 0.05f, 0.2f);

        GameObject yAxis = GameObject.CreatePrimitive(PrimitiveType.Cube);
        yAxis.name = "yAxis";
        yAxis.transform.position = new Vector3(0, 0, 0);
        yAxis.transform.localScale = new Vector3(0.05f, 20, 0.2f);

        // Draw small markers along the axisBars (10 on each direaction).
        for (int i = -10; i <= 10; i++)
        {
            GameObject xMarker = GameObject.CreatePrimitive(PrimitiveType.Cube);
            string markerXName = string.Format("Marker X: " + i);
            xMarker.name = markerXName;
            xMarker.transform.position = new Vector3(i, 0, 0);
            xMarker.transform.localScale = new Vector3(0.05f, 1, 0.2f);

            GameObject yMarker = GameObject.CreatePrimitive(PrimitiveType.Cube);
            string markerYName = string.Format("Marker Y: " + i);
            yMarker.name = markerYName;
            yMarker.transform.position = new Vector3(0, i, 0);
            yMarker.transform.localScale = new Vector3(1, 0.05f, 0.2f);
        }
    }

    public void DrawClickablePins()
    {
        efficientlyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        efficientlyPin.name = "efficientlyPin";
        // Attach "DragPin" script to the clickalble pin
        efficientlyPin.AddComponent(Type.GetType("DragPin"));
        efficientlyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        efficientlyPin.transform.position = new Vector3(0, chartParams.Efficiently, 0);
        efficientlyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);


        stealthlyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        stealthlyPin.name = "stealthelyPin";
        // Attach "DragPin" script to the clickalble pin
        stealthlyPin.AddComponent(Type.GetType("DragPin"));
        stealthlyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        stealthlyPin.transform.position = new Vector3(chartParams.Stealthly, 0, 0);
        stealthlyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);


        quicklyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        quicklyPin.name = "quicklyPin";
        // Attach "DragPin" script to the clickalble pin
        quicklyPin.AddComponent(Type.GetType("DragPin"));
        quicklyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        quicklyPin.transform.position = new Vector3(0, -1 * chartParams.Quickly, 0);
        quicklyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);


        safelyPin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        safelyPin.name = "safelyPin";
        // Attach "DragPin" script to the clickalble pin
        safelyPin.AddComponent(Type.GetType("DragPin"));
        safelyPin.GetComponent<MeshRenderer>().material = clickMaterial;
        safelyPin.transform.position = new Vector3(-1 * chartParams.Safely, 0, 0);
        safelyPin.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }

    // Update the position of the clickable pins
    public void UpdateClickableObjects()
    {
        efficientlyPin.transform.position = new Vector3(0, chartParams.Efficiently, 0);
        stealthlyPin.transform.position = new Vector3(chartParams.Stealthly, 0, 0);
        quicklyPin.transform.position = new Vector3(0, -1 * chartParams.Quickly, 0);
        safelyPin.transform.position = new Vector3(-1 * chartParams.Safely, 0, 0);
    }

    // Reads the "data.txt" file and loads the parameters to ChartParams class
    public void LoadData()
    {
        string dataFileName = "data.txt";
        string filePath = Path.Combine(Application.streamingAssetsPath, dataFileName);
        int[] parameters = new int[4];
        string line;
        int counter = 0;

        StreamReader file = new StreamReader(filePath); 

        if (File.Exists(filePath))
        {
            while ((line = file.ReadLine()) != null)
            {
                parameters[counter] = int.Parse(line);
                Debug.Log(parameters[counter]);
                counter++;
            }

            chartParams.Efficiently = parameters[0];
            chartParams.Stealthly = parameters[1];
            chartParams.Quickly = parameters[2];
            chartParams.Safely = parameters[3];

            file.Close();
            Debug.Log("There were " + counter + " lines.");
            Debug.Log("\n");
        }
    }

    // One "SET INTENT" btn is pressed, create "intentData.txt" file.
    public void CreateExternalFiles()
    {
        string dataFileName = "data.txt";
        string intentDataFileName = "intentData.txt";
        string dataFilePath = Path.Combine(Application.streamingAssetsPath, dataFileName);
        string intentFilePath = Path.Combine(Application.streamingAssetsPath, intentDataFileName);

        try
        {
            if (File.Exists(dataFilePath))
            {
                File.Delete(dataFilePath);
            }

            if (File.Exists(intentFilePath))
            {
                File.Delete(intentFilePath);
            }

            using (FileStream fs = File.Create(dataFilePath))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(chartParams.Efficiently 
                    + Environment.NewLine + chartParams.Stealthly 
                    + Environment.NewLine + chartParams.Quickly 
                    + Environment.NewLine + chartParams.Safely);
                fs.Write(info, 0, info.Length);
            }

            using (FileStream fs = File.Create(intentFilePath))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(chartParams.Efficiently
                    + Environment.NewLine + chartParams.Stealthly
                    + Environment.NewLine + chartParams.Quickly
                    + Environment.NewLine + chartParams.Safely);
                fs.Write(info, 0, info.Length);
            }
        }

        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }

    // Create "SET INTENT" button
    void OnGUI()
    {
        if (!btnTexture)
        {
            Debug.LogError("Please assign a texture on the inspector");
            return;
        }

        if (GUI.Button(new Rect(700, 50, 100, 40), "SET INTENT"))
        {
            CreateExternalFiles();
            chartParams.IsLocked = true;
            Debug.Log("Clicked the button with text");
        }
    }
}
