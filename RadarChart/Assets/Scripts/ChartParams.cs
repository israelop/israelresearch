﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ChartParams is a Singleton class in order to make sure we create only one instance of it.
public sealed class ChartParams {
    private int efficiently;
    private int stealthly;
    private int quickly;
    private int safely;

    private Boolean isLocked;

    private static ChartParams instance = null;

    public static ChartParams GetInstance
    {
        get
        {
            if (instance == null)
                instance = new ChartParams();
            return instance;
        }
    }

    private ChartParams()
    {
        this.Efficiently = 8;
        this.Stealthly = 8;
        this.Quickly = 4;
        this.Safely = 2;
        this.isLocked = false;
    }

    public int Efficiently
    {
        get
        {
            return efficiently;
        }

        set
        {
            efficiently = value;
        }
    }

    public int Stealthly
    {
        get
        {
            return stealthly;
        }

        set
        {
            stealthly = value;
        }
    }

    public int Quickly
    {
        get
        {
            return quickly;
        }

        set
        {
            quickly = value;
        }
    }

    public int Safely
    {
        get
        {
            return safely;
        }

        set
        {
            safely = value;
        }
    }

    public Boolean IsLocked
    {
        get
        {
            return isLocked;
        }

        set
        {
            isLocked = value;
        }
    }


}
