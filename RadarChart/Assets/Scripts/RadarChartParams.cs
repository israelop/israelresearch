﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class RadioChartParams {
    private int efficiency;
    private int stealthly;
    private int quickly;
    private int unknown;

    private Boolean isLocked;

    private static RadioChartParams instance = null;

    public static RadioChartParams GetInstance
    {
        get
        {
            if (instance == null)
                instance = new RadioChartParams();
            return instance;
        }
    }

    private RadioChartParams()
    {
        this.Efficiency = 8;
        this.Stealthly = 8;
        this.Quickly = 4;
        this.Unknown = 2;
        this.isLocked = false;
    }

    public int Efficiency
    {
        get
        {
            return efficiency;
        }

        set
        {
            efficiency = value;
        }
    }

    public int Stealthly
    {
        get
        {
            return stealthly;
        }

        set
        {
            stealthly = value;
        }
    }

    public int Quickly
    {
        get
        {
            return quickly;
        }

        set
        {
            quickly = value;
        }
    }

    public int Unknown
    {
        get
        {
            return unknown;
        }

        set
        {
            unknown = value;
        }
    }

    public Boolean IsLocked
    {
        get
        {
            return isLocked;
        }

        set
        {
            isLocked = value;
        }
    }


}
