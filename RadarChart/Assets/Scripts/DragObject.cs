﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    private Camera cam;
    private Vector3 point;
    private RadioChartParams radioParams = RadioChartParams.GetInstance;
    private TestMesh testMesh;


    void Start()
    {
        cam = Camera.main;
    }

    private void OnMouseDown()
    {
        Debug.Log("Clicking");
    }

    private void OnMouseDrag()
    {
        if (!radioParams.IsLocked)
        {
            Debug.Log("Dragging");

            if (name.Equals("efficiencyPin"))
            {
                if (point.y > 10)
                {
                    transform.position = new Vector3(transform.position.x, 10, transform.position.z);
                }
                else if (point.y < 0)
                {
                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                }
                else if (point.y >= 0 && point.y <= 10)
                {
                    transform.position = new Vector3(transform.position.x, (int)point.y, transform.position.z);
                }

                radioParams.Efficiency = (int)transform.position.y;
            }
            else if (name.Equals("stealthlyPin"))
            {
                if (point.x > 10)
                {
                    transform.position = new Vector3(10, transform.position.y, transform.position.z);
                }
                else if (point.x < 0)
                {
                    transform.position = new Vector3(0, transform.position.y, transform.position.z);
                }
                else if (point.x >= 0 && point.x <= 10)
                {
                    transform.position = new Vector3((int)point.x, transform.position.y, transform.position.z);
                }

                radioParams.Stealthly = (int)transform.position.x;
            }
            else if (name.Equals("quicklyPin"))
            {
                if (point.y > 0)
                {
                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                }
                else if (point.y < -10)
                {
                    transform.position = new Vector3(transform.position.x, -10, transform.position.z);
                }
                else if (point.y >= -10 && point.y <= 0)
                {
                    transform.position = new Vector3(transform.position.x, (int)point.y, transform.position.z);
                }

                radioParams.Quickly = -(int)transform.position.y;
            }
            else if (name.Equals("safetyPin"))
            {
                if (point.x > 0)
                {
                    transform.position = new Vector3(0, transform.position.y, transform.position.z);
                }
                else if (point.x < -10)
                {
                    transform.position = new Vector3(-10, transform.position.y, transform.position.z);
                }
                else if (point.x >= -10 && point.x <= 0)
                {
                    transform.position = new Vector3((int)point.x, transform.position.y, transform.position.z);
                }

                radioParams.Unknown = -(int)transform.position.x;
            }
        }
    }

    void OnGUI()
    {
        point = new Vector3();
        Event currentEvent = Event.current;
        Vector2 mousePos = new Vector2();

        mousePos.x = currentEvent.mousePosition.x;
        mousePos.y = cam.pixelHeight - currentEvent.mousePosition.y;

        point = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 25));

        GUILayout.BeginArea(new Rect(20, 20, 300, 120));
        GUILayout.Label("Mouse position (x, y, z): " + point.ToString("F3"));
        GUILayout.EndArea();
    }
}
