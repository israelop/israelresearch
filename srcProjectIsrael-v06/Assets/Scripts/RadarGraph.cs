﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RadarGraph : MonoBehaviour {

    public Transform axisBar;
    public int axisNum;
    public Material material;

    private float angleNum;
    private Dictionary<string, float> data = new Dictionary<string, float>();
    private List<Vector3> points = new List<Vector3>();

    // Use this for initialization
    void Start() {
        float rotationAngle = 0;
        angleNum = 360 / axisNum;

        //Function for acquiring data. Just pulls Random values, atm.
        getData();

        //Adds in and sets values of Axis
        for (int i = 0; i < axisNum; i++)
        {
            //Instantiates Axis
            Transform axis = Instantiate(axisBar, transform.position, Quaternion.identity) as Transform;

            //Grabs current data Item
            var item = data.ElementAt(i);

            GraphAxis gAxis = axis.gameObject.GetComponent<GraphAxis>();
            Vector3 pos = gAxis.marker.localPosition;
            pos.y = (gAxis.top.position.y - gAxis.transform.position.y) * (item.Value / 100);
            gAxis.marker.localPosition = pos;

            //Temp vector3 for multiple purposes
            Vector3 temp = axis.eulerAngles;

            //Assign Axis angle
            temp.z = rotationAngle;
            axis.eulerAngles = temp;

            //Straitens out Text labels if Axis have them (Optional)
            temp = gAxis.text.transform.eulerAngles;
            temp.z = 0;
            gAxis.text.transform.eulerAngles = temp;

            //Adjust Text position as to not obstruct data (Optional)
            TextMesh textMesh = gAxis.text;
            temp = textMesh.transform.position;

            if (rotationAngle < 180 && rotationAngle != 0)
            {
                temp.x -= ((textMesh.transform.position.x / 2) + .01f);
            }
            else if(rotationAngle > 180)
            {
                temp.x += ((textMesh.transform.position.x / 2) + .01f);
            }

            textMesh.transform.position = temp;
            textMesh.text = item.Key;

            //Adds Marker's position to the list of data points to be used as a vertex
            points.Add(gAxis.marker.position);

            //Adjusts angle of rotation for next access and parents current Axis
            rotationAngle += angleNum;
            axis.parent = this.transform;
        }

        showGraph();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void getData()
    {
        for(int i = 0; i < axisNum; i++)
        {
            data.Add(("Param " + (i + 1)), Random.Range(25, 93));
        }
    }

    void showGraph()
    {
        //GameObject mesh is assigned
        GameObject chart = new GameObject("Block");
        //Mesh to be used as a Graph
        Mesh newMesh = new Mesh();
        //Filter and Rendering Neccesary to display Mesh
        chart.AddComponent<MeshFilter>();
        chart.AddComponent<MeshRenderer>();

        //An array of the vertex points (as Vector3s) to be used for mesh
        Vector3[] verts = new Vector3[points.Count];

        //convets list of points to array (Mesh only accpts arrays
        for (int i = 0; i < points.Count; i++)
        {
            verts[i] = points[i];
        }

        //Assign UVs to Mesh
        Vector2[] uvs = new Vector2[newMesh.vertices.Length];

        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(newMesh.vertices[i].x, newMesh.vertices[i].z);
        }

        newMesh.uv = uvs;

        //New Array for Mesh Tris
        int[] tris = new int[(axisNum) * 3];

        int p1 = 1; //first point of triangle other than origin (0) going counter clockwise
        int p2 = 2; //second point of triangle other than origin (0) going counter clockwise
        int pLast = p1; //Value for making the Last triangle (and therefore closing the mesh)

        //Assigns Tris in a clockwise fashion (counterchlowise displays the mesh
        for (int i = 0; i < (axisNum - 1) * 3; i += 3)
        {
            tris[i] = 0;
            tris[i + 1] = p2;
            tris[i + 2] = p1;

            ++p1;
            ++p2;
        }

        //Assigns end point of triangles, "closing" the mesh
        tris[((axisNum) * 3) - 3] = 0;
        tris[((axisNum) * 3) - 1] = p1;
        tris[((axisNum) * 3) - 2] = pLast;

        //Assigns Tris (I think we got it by now)
        newMesh.triangles = tris;

        //Assigning the mesh to gameObject and other fine details
        newMesh.RecalculateNormals();
        chart.GetComponent<MeshFilter>().mesh = newMesh;
        //?? chart.renderer.material = mater;
        chart.AddComponent<MeshCollider>();

        //Parent new graph to this Script's owner
        chart.transform.parent = gameObject.transform;
    }
}
