﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using UnityEngine;
using HoloToolkit.Unity;

public abstract class InteractibleAction : MonoBehaviour
{
    [Tooltip("Drag the Tagalong prefab asset you want to display.")]
    public GameObject ObjectToTagalong;

    void PerformTagAlong()
    {
        if(ObjectToTagalong == null)
        {
            return;
        }

        GameObject existingTagAlong = GameObject.FindGameObjectWithTag("TagAlong");
        if(existingTagAlong != null)
        {
            return;
        }

        GameObject instantiateObjectToTagAlong = GameObject.Instantiate(ObjectToTagalong);

        instantiateObjectToTagAlong.SetActive(true);

        instantiateObjectToTagAlong.AddComponent<Billboard>();
        instantiateObjectToTagAlong.AddComponent<SimpleTagalong>();

    }
}