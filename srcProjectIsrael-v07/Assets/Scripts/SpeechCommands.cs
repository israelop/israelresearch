﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class SpeechCommands : MonoBehaviour {
    KeywordRecognizer keywordsRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    private SpeechEvents speechEvents;
    string argTargetName;
    string argChangingDecision;
    
	// Use this for initialization
	void Start () {
        keywords.Add("go", () =>
        {
            argTargetName = "home";
            speechEvents.InitialState(argTargetName);
        });

        keywords.Add("to the door", () =>
        {
            argTargetName = "door";
            speechEvents.InitialState(argTargetName);
        });

        keywords.Add("to the tower", () => 
        {
            argTargetName = "tower";
            speechEvents.InitialState(argTargetName);
        });

        keywords.Add("yes", () =>
        {
            argChangingDecision = "yes";
            speechEvents.ChangingState(argChangingDecision);
        });

        keywords.Add("no", () =>
        {
            argChangingDecision = "no";
            speechEvents.ChangingState(argChangingDecision);
        });

        keywordsRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordsRecognizer.OnPhraseRecognized += KeywordsRecognizerOnPhraseRecognized;
        keywordsRecognizer.Start();
    }

    void KeywordsRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if(keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    void Awake()
    {
        speechEvents = GetComponent<SpeechEvents>();    
    }
}
