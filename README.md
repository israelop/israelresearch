# Effect of a Mixed Reality Interface on the User’s Intent

## Abstract

A user interface is the visual part of a computer application through which the user interacts with a program. 3D simulators and/or Mixed Reality interfaces are already gaining momentum as powerful tools for teaching and workforce training. Efficient use of these interfaces may depend on human capabilities for spatial cognition. However, it is unclear whether a 3D display, compared to a 2D display, enhances the user performance of an intent-based task. The purpose of this project is to better understand the effects of a 3D-mixed-reality interface in executing or carrying out user intent. To accomplish this goal, we are adapting a 2D interface of a path-planning application into a 3D mixed-reality solution. We plan to compare the user behavior in both the environments.

- 2D Intent-based Robotic Path Replanning
![image](https://drive.google.com/uc?export=view&id=1nLOItcV7kA9rMBTc0IsF9Xz9cE2Vq1kS)

### Demo Video

- To see the DEMO of the project deployed into the Hololens, click [here](https://drive.google.com/file/d/1ndm5y6TVXD8rqDvBo-RjFbIiISbYB92D/view?usp=sharing). The red cube represents the goal position for the tank..

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Download and Install
```
- Unity 2017.4.0f1 (64-bit) or a newer version.
- Visual Studio 2017
```

## Deployment
For more detailed instructions click [here](https://docs.microsoft.com/en-us/windows/mixed-reality/holograms-100)

```
- Open Unity
- Open Project: srcProjectIsrael-v06
- Wait until the project finish loading in Unity

In Unity:
- Open File > Build Settings window.
- Click Add Open Scenes to add the scene.
- Change Platform to Universal Windows Platform and click Switch Platform.
- In Windows Store settings ensure, SDK is Universal 10.
- For Target device, leave to Any Device for occluded displays or switch to HoloLens.
- UWP Build Type should be D3D.
- UWP SDK could be left at Latest installed.
- Check Unity C# Projects under Debugging.
- Click Build.
- In the file explorer, click New Folder and name the folder "App".
- With the App folder selected, click the Select Folder button.
- When Unity is done building, a Windows File Explorer window will appear.
- Open the App folder in file explorer.
- Open the generated Visual Studio solution (MixedRealityIntroduction.sln in this example)
- Wait unitl the solution is loaded to Visual Studio

In Visual Studio:
- For HoloLens, click on the arrow next to the Local Machine button, and change the deployment target to Device.
- For targeting occluded devices attached to your PC, keep the setting to Local Machine. Ensure you have the Mixed Reality Portal running.
- Click Debug > Start without debugging.
```

## Authors

* **Israel Lopez**
