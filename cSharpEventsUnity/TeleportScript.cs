﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportScript : MonoBehaviour {

    void OnEnable()
    {
        //To Subscribe an method to the event.
        EventManager.OnClicked += Teleport;
    }

    void OnDisable()
    {
        //To Unsubscribe the method to the event.
        EventManager.OnClicked -= Teleport;
    }

    void Teleport()
    {
        Vector3 pos = transform.position;
        pos.y = Random.Range(1.0f, 3.0f);
        transform.position = pos;
    }
}
