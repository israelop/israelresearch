﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerExample : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public void OnBeginDrag(PointerEventData data)
    {
        Debug.Log("OnBeginDrag called.");
    }

    public void OnDrag(PointerEventData data)
    {
        Debug.Log("OnDrag called.");
    }

    public void OnDrop(PointerEventData data)
    {
        Debug.Log("OnDrop called.");
    }

    public void OnEndDrag(PointerEventData data)
    {
        Debug.Log("OnEndDrag called.");
    }

    public void OnInitializePotentialDrag(PointerEventData data)
    {
        Debug.Log("OnInitializePotentialDrag called.");
    }

    public void OnMove(AxisEventData data)
    {
        Debug.Log("OnMove called.");
    }
    /*
    public override void OnPointerClick(PointerEventData data)
    {
        //Debug.Log("OnPointerClick called.");
    }

    public override void OnPointerDown(PointerEventData data)
    {
        //Debug.Log("OnPointerDown called.");
    }

    public override void OnPointerEnter(PointerEventData data)
    {
       // Debug.Log("OnPointerEnter called.");
    }

    public override void OnPointerExit(PointerEventData data)
    {
       // Debug.Log("OnPointerExit called.");
    }

    public override void OnPointerUp(PointerEventData data)
    {
        //Debug.Log("OnPointerUp called.");
    }
    */
}