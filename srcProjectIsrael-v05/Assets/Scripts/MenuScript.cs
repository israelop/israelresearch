﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour {

    public enum MenuStates
    {
        NavigationState,
        ParametersState
    }

    public GameObject NavigationPanel;
    public GameObject ParametersPanel;
    public GameObject VoiceCommandsPanel;
    public MenuStates currentState;

    void Awake()
    {
        currentState = MenuStates.NavigationState;
    }

    void Update()
    {
        switch (currentState)
        {
            case MenuStates.NavigationState:
                NavigationPanel.SetActive(true);
                ParametersPanel.SetActive(false);
                VoiceCommandsPanel.SetActive(true);
                break;
            case MenuStates.ParametersState:
                NavigationPanel.SetActive(false);
                ParametersPanel.SetActive(true);
                VoiceCommandsPanel.SetActive(false);
                break;
        }    
    }


    public void OnParameters()
    {
        currentState = MenuStates.ParametersState;
    }

    public void OnBackToMenu()
    {
        currentState = MenuStates.NavigationState;
    }
}
