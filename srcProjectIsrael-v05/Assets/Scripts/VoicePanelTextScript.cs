﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VoicePanelTextScript : MonoBehaviour {
    [SerializeField]
    private Text instruction = null;

    public void Start () {
        instruction.text = "Commands:" + "\n\t- Go Home!" + "\n\t- To the door!" + "\n\t- To the tower!";
    }

    private void OnEnable()
    {
        SpeechEvents.OnSpeechHomeEvent += InitialCommands;
        SpeechEvents.OnSpeechDoorEvent += InitialCommands;
        SpeechEvents.OnSpeechTowerEvent += InitialCommands;
        SpeechEvents.OnChangingStateEvent += ChangingStateCommands;
        SpeechEvents.OnYesPanel += InitialCommands;
    }

    private void OnDisable()
    {
        SpeechEvents.OnSpeechHomeEvent -= InitialCommands;
        SpeechEvents.OnSpeechHomeEvent -= InitialCommands;
        SpeechEvents.OnSpeechTowerEvent -= InitialCommands;
        SpeechEvents.OnChangingStateEvent -= ChangingStateCommands;
        SpeechEvents.OnYesPanel -= InitialCommands;
    }

    public void InitialCommands()
    {
        instruction.text = "Commands:" + "\n\t- Go Home!" + "\n\t- To the door!" + "\n\t- To the tower!";
    }

    void ChangingStateCommands()
    {
        instruction.text = "Commands:" + "\n\t- Yes" + "\n\t- No!";
    }
}
