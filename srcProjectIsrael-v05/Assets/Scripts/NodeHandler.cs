﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NodeHandler : MonoBehaviour {
    private BoxCollider2D bc;
    private Rigidbody2D rb;

    void Awake()
    {
        SpriteRenderer sprRend = gameObject.AddComponent<SpriteRenderer>() as SpriteRenderer;
        //sprRend.color = new Color(0.9f, 0.9f, 0.9f, 1.0f);

        bc = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
        bc.size = new Vector2(1.3f, 1.3f);
        bc.isTrigger = true;

        rb = gameObject.AddComponent<Rigidbody2D>() as Rigidbody2D;
        //rb.bodyType = RigidbodyType2D.Kinematic;
    }

    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("logo");
        gameObject.transform.Translate(4.0f, 0.0f, 0.0f);
        gameObject.transform.localScale = new Vector2(2.0f, 2.0f);
    }

    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    Transform startParent;
    public GameObject button;

    //Region IBeginDragHandeler implementation
    public void OnBeginDrag(PointerEventData eventData)
    {
        GameObject b = Instantiate(button);
        b.transform.parent = transform.parent;
        itemBeingDragged = b;
        startPosition = transform.position;
        //startParent = transform.parent;
        //Debug.Log("OnBeginDrag");
    }

    //Region IDragHandeler implementation
    public void OnDrag(PointerEventData eventData)
    {
        itemBeingDragged.transform.position = Input.mousePosition;
        //Debug.Log("OnDrag");
    }

    //Region IEndDragHandeler implementation
    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEndDrag");
        itemBeingDragged = null;
        //if(transform.parent != startParent)
        //{
        //   transform.position = startPosition;
        // }
    }

    void OnTriggerEnter(Collider other)
    {
        //if(other.gameObject.tag == "StealthilyModeButton")
        //{
        //Debug.Log("Touched");
        Destroy(this.gameObject);
        //}
    }
}
