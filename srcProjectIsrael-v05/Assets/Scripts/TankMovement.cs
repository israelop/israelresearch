﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour {
    Vector3 lookAtTarget;
    Vector3 targetPos;
    Quaternion playerRot;
    float rotSpeed = 5;
    float speed = 10;
    bool moving = false;

    private void OnEnable()
    {
        SpeechEvents.OnSpeechHomeEvent += SetTargetHome;
        SpeechEvents.OnSpeechDoorEvent += SetTargetDoor;
        SpeechEvents.OnSpeechTowerEvent += SetTargetTower;
    }

    private void OnDisable()
    {
        SpeechEvents.OnSpeechHomeEvent -= SetTargetHome;
        SpeechEvents.OnSpeechHomeEvent -= SetTargetDoor;
        SpeechEvents.OnSpeechTowerEvent -= SetTargetTower;
    }

    void SetTargetHome()
    {
        SetTarget(new Vector3(171.6f, 0.4187773f, 170.3f));
    }

    void SetTargetDoor()
    {
        SetTarget(new Vector3(23.2f, 0.4187773f, 156.8f));
    }

    void SetTargetTower()
    {
        SetTarget(new Vector3(174f, 0.4187773f, 25.9f));
    }

    public void SetTarget(Vector3 argTargetPos)
    {
        lookAtTarget = new Vector3(argTargetPos.x - transform.position.x,
            transform.position.y, argTargetPos.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        targetPos = argTargetPos;
        moving = true;
    }

	void Update () {
        if (moving)
        {
            Move();
        }
	}

    void Move()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot,
            rotSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPos,
            speed * Time.deltaTime);
        if(transform.position == targetPos)
        {
            moving = false;
        }
    }

    public bool GetMovingCond()
    {
        return moving;
    }

    public void SetMovingCond(bool moving)
    {
        this.moving = moving;
    }
}
