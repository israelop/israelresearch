﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject newNode_beingDragged;
    public GameObject button;
    public GameObject canvas;
    Vector3 startPosition;
    //RaycastHit hit;

    //Line Renderer
    public static LineRenderer line;


    public UnityEngine.UI.Extensions.UILineRenderer LineRenderer;
    public List<Vector2> pointlist = new List<Vector2>();
    //public GameObject UILine_prefab;

    public void OnBeginDrag(PointerEventData data)
    {
        GameObject node_prefab = Instantiate(button);
        node_prefab.transform.parent = transform.parent;
        newNode_beingDragged = node_prefab;
        startPosition = transform.position;
        canvas = transform.parent.gameObject;

        //GameObject line_prefab = Instantiate(UILine_prefab);

        //Line Renderer
        LineRenderer lineRenderer = canvas.AddComponent<LineRenderer>();
        lineRenderer.transform.parent = transform.parent;
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        //lineRenderer.SetWidth(.45f, .45f);
        lineRenderer.widthMultiplier = 1.5f;
        //line = lineRenderer;
        lineRenderer.SetPosition(0, startPosition);
        //lineRenderer.SetPosition(1, new Vector3(40.5f, 40.5f, 50.5f));

        var point = new Vector2()
        {
            x = startPosition.x,
            y = startPosition.y
        };
        pointlist.Add(point);
    }

    public void OnDrag(PointerEventData data)
    {
        newNode_beingDragged.transform.position = Input.mousePosition;
        //Physics.Raycast(startPosition, Input.mousePosition, out hit, Mathf.Infinity);

        //Line Renderer
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
       
        lineRenderer.SetPosition(1, Input.mousePosition);

        var point = new Vector2()
        {
            x = Input.mousePosition.x,
            y = Input.mousePosition.y
        };
        pointlist.Add(point);
        LineRenderer.Points = pointlist.ToArray();

        Debug.Log("OnDrag");
    }

    public void OnDrop(PointerEventData data)
    {
        Debug.Log("OnDrop called.");
    }

    public void OnEndDrag(PointerEventData data)
        
    {
        newNode_beingDragged = null;
        Debug.Log("OnEndDrag called.");
    }

    public void OnInitializePotentialDrag(PointerEventData data)
    {
        Debug.Log("OnInitializePotentialDrag called.");
    }

    public void OnMove(AxisEventData data)
    {
        Debug.Log("OnMove called.");
    }
}
