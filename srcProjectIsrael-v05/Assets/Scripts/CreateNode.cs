﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNode : MonoBehaviour {
    LineRenderer lineRenderer;
    // Use this for initialization
    public void OnClick() {
        //lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.widthMultiplier = 0.2f;
        lineRenderer.positionCount = 2; //The number of vertices
        lineRenderer.SetPosition(0, Input.mousePosition);
        Debug.Log("Created LindeRenderer");
	}

    // Update is called once per frame
    void Update()
    {
        if (lineRenderer == null)
        {
            lineRenderer.SetPosition(1, Input.mousePosition);
            Debug.Log("Update LineRenderer");
        }
    }
}
