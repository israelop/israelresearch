﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverPad : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Object Entered the trigger");
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Object is within trigger");
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("Object is withing 2D trigger");
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Object Exited the trigger");
    }
}
