using UnityEngine;

// Destroy owning GameObject if any collider with a specific tag is trespassing
public class DestroyOnTrigger : MonoBehaviour
{
    public string m_Tag = "StealthilyModeButton";

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Touched!!");
        if (other.gameObject.tag == m_Tag)
            Destroy(gameObject);
    }
}
