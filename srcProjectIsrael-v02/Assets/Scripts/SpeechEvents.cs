﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechEvents : MonoBehaviour {
    public delegate void HomeEvent();
    public static event HomeEvent OnSpeechHomeEvent;
    public delegate void DoorEvent();
    public static event DoorEvent OnSpeechDoorEvent;
    public delegate void TowerEvent();
    public static event TowerEvent OnSpeechTowerEvent;
    public delegate void ChangingStateEvent();
    public static event TowerEvent OnChangingStateEvent;
    public delegate void YesPanel();
    public static event YesPanel OnYesPanel;

    TankMovement tankMovement;
    public GameObject panel;
    PanelTextScript panelTextScript;
    string originalTarget;
    bool changingStateEntered = false;
       
    void SetEvent(string argTargetName)
    {
        Debug.Log("You said " + argTargetName);
        switch (argTargetName)
        {
            case "home":
                if (OnSpeechHomeEvent != null)
                    OnSpeechHomeEvent();
                break;
            case "door":
                if (OnSpeechDoorEvent != null)
                    OnSpeechDoorEvent();
                break;
            case "tower":
                if (OnSpeechTowerEvent != null)
                    OnSpeechTowerEvent();
                break;
            default:
                break;
        }
    }

    public void InitialState(string argTargetName)
    {
        if (tankMovement.GetMovingCond())
        {
            tankMovement.SetMovingCond(false);
            changingStateEntered = true;
            OnChangingStateEvent();
        }
        else
        {
            originalTarget = argTargetName;
            SetEvent(argTargetName);
        }
    }

    public void ChangingState(string argChangingDecision)
    {
        if (changingStateEntered)
        {
            changingStateEntered = false;
            if (argChangingDecision == "yes")
            {
                OnYesPanel();
            }
            else if(argChangingDecision == "no")
            {
                SetEvent(originalTarget);
            }
        }
    }

    void Awake()
    {
        tankMovement = GetComponent<TankMovement>();
    }
}
