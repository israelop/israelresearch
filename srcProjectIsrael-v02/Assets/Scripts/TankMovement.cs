﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour {
    Vector3 lookAtTarget;
    Vector3 targetPos;
    Quaternion playerRot;
    float rotSpeed = 5;
    float speed = 10;
    bool moving = false;

    private void OnEnable()
    {
        SpeechEvents.OnSpeechHomeEvent += SetTargetHome;
        SpeechEvents.OnSpeechDoorEvent += SetTargetDoor;
        SpeechEvents.OnSpeechTowerEvent += SetTargetTower;
    }

    private void OnDisable()
    {
        SpeechEvents.OnSpeechHomeEvent -= SetTargetHome;
        SpeechEvents.OnSpeechHomeEvent -= SetTargetDoor;
        SpeechEvents.OnSpeechTowerEvent -= SetTargetTower;
    }

    void SetTargetHome()
    {
        SetTarget(new Vector3(16.6f, 0.4187773f, 84.3f));
    }

    void SetTargetDoor()
    {
        SetTarget(new Vector3(86.1f, 0.4187773f, 82.7f));
    }

    void SetTargetTower()
    {
        SetTarget(new Vector3(69.17f, 0.4187773f, 21.59f));
    }

    public void SetTarget(Vector3 argTargetPos)
    {
        lookAtTarget = new Vector3(argTargetPos.x - transform.position.x,
            transform.position.y, argTargetPos.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        targetPos = argTargetPos;
        moving = true;
    }

	void Update () {
        if (moving)
        {
            Move();
        }
	}

    void Move()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot,
            rotSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPos,
            speed * Time.deltaTime);
        if(transform.position == targetPos)
        {
            moving = false;
        }
    }

    public bool GetMovingCond()
    {
        return moving;
    }

    public void SetMovingCond(bool moving)
    {
        this.moving = moving;
    }
}
