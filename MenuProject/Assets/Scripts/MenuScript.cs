﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    public enum MenuStates
    {
        Main,
        Options
    }

    public MenuStates currentState;
    public GameObject mainMenu;
    public GameObject optionMenu;

    void Awake()
    {
        currentState = MenuStates.Main;
    }

    void Update()
    {
        switch (currentState)
        {
            case MenuStates.Main:
                //Setse active gameobject for main menu
                mainMenu.SetActive(true);
                optionMenu.SetActive(false);
                break;
            case MenuStates.Options:
                //Sets active gameobject for options menu
                mainMenu.SetActive(false);
                optionMenu.SetActive(true);
                break;
        }
    }

    public void OnStartGame()
    {
        Debug.Log("You pressed the start game button");
    }

    public void OnOptions()
    {
        Debug.Log("You've clicked options");
        currentState = MenuStates.Options;
    }

    public void OnWindowedMode()
    {
        Debug.Log("You want to play on windows mode");
    }

    public void OnMainMenu()
    {
        Debug.Log("Go back to the main menu");
        currentState = MenuStates.Main;
    }
}
