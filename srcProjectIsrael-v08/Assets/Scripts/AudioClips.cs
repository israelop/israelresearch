﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClips : MonoBehaviour {

    public AudioClip audioGoGoGo;
    public AudioClip makeUpYourMind;
    public AudioClip iHaventGotAllDay;
    public AudioClip areYouSureChangeDest;
    public float volume;
    AudioSource audioClip;

    void Start()
    {
        audioClip = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        SpeechEvents.OnSpeechHomeEvent += HomeClip;
        SpeechEvents.OnSpeechDoorEvent += DoorClip;
        SpeechEvents.OnSpeechTowerEvent += TowerClip;
        SpeechEvents.OnChangingStateEvent += ChangingState;
    }

    private void OnDisable()
    {
        SpeechEvents.OnSpeechHomeEvent -= HomeClip;
        SpeechEvents.OnSpeechHomeEvent -= DoorClip;
        SpeechEvents.OnSpeechTowerEvent -= TowerClip;
        SpeechEvents.OnChangingStateEvent -= ChangingState;
    }

    void HomeClip()
    {
        audioClip.PlayOneShot(audioGoGoGo, volume);
    }

    void DoorClip()
    {
        audioClip.PlayOneShot(makeUpYourMind, volume);
    }

    void TowerClip()
    {
        audioClip.PlayOneShot(iHaventGotAllDay, volume);
    }

    void ChangingState()
    {
        audioClip.PlayOneShot(areYouSureChangeDest, volume);
    }
}
