﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;
using UnityEngine;
using System.Linq;


public class SpeechRecognition : MonoBehaviour {
    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    Vector3 targetPosition;
    Vector3 lookAtTarget;
    Quaternion playerRot;
    float rotSpeed = 5;
    float speed = 10;
    bool moving = false;
    public Vector3 homePos = new Vector3(16.6f, 0.4187773f, 84.3f);
    public Vector3 doorPos = new Vector3(86.1f, 0.4187773f, 82.7f);
    public Vector3 towerPos = new Vector3(68.5f, 0.4187773f, 19.6f);
    //public Vector3 oldPosition;
    //For the audio Clip
    public AudioClip audioGoGoGo;
    public AudioClip makeUpYourMind;
    public AudioClip iHaventGotAllDay;
    public AudioClip areYouSureChangeDest;
    public float volume;
    AudioSource audio;
    bool answer = false;
    
    void Start()
    {
        keywords.Add("go home", () =>
        {
            HomeCalled();
        });

        keywords.Add("to the door", () =>
        {
            DoorCalled();
        });

        keywords.Add("to the tower", () =>
        {
            TowerCalled();
        });
        
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keywordRecognizer.Start();
        //for the audio clip
        audio = GetComponent<AudioSource>();

    }

    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    void HomeCalled()
    {
        Debug.Log("You said Home");
        audio.PlayOneShot(audioGoGoGo, volume);
        //oldPosition = transform.position;
        targetPosition = homePos;
        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
            transform.position.y,
            targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        moving = true;
    }

    void DoorCalled()
    {
        Debug.Log("You said Door");
        audio.PlayOneShot(makeUpYourMind, volume);
        //oldPosition = transform.position;
        targetPosition = doorPos;
        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
            transform.position.y,
            targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        moving = true;
    }

    void TowerCalled()
    {
        Debug.Log("You said Tower");
        audio.PlayOneShot(iHaventGotAllDay, volume);
        //oldPosition = transform.position;
        targetPosition = towerPos;
        lookAtTarget = new Vector3(targetPosition.x - transform.position.x,
            transform.position.y,
            targetPosition.z - transform.position.z);
        playerRot = Quaternion.LookRotation(lookAtTarget);
        moving = true;
    }

    void Update()
    {
        if (moving)
        {
            Move();
        }
    }

    void Move()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, playerRot,
            rotSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition,
            speed * Time.deltaTime);
        if (transform.position == targetPosition)
        {
            moving = false;
        }
    }
}
